# coding=utf-8

"""
A name server decorator with the registered name augmented with object
types based on a given object name and object URI. The interface
requires that the given object URI can be found from the local daemon.
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014-2015"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import time
import logging

from .config import Pyro4
import numpy as np


class TypedNameServer(object):
    """A typed nameserver wrapper over the standard Pyro4 name
    server. Names registered in the Pyro4 name server are augmented by
    object types, making it possible to do yellow-pages. The life
    cycle of this facade regularly enforces the name to object mapping
    of objects that have registered. This make up a resilience measure
    against recycling of the Pyro4 name server.
    """

    def __init__(self, pyro_name_server, daemon, log_level='INFO'):
        self._pns = pyro_name_server
        self._dmn = daemon
        self._log = logging.getLogger(name=self.__class__.__name__)
        self._log.setLevel(log_level)

    def _encode_name(self, obj, obj_name):
        """For the given object and the given object name, return the object
        typed name to enter for the raw Pyro name server. The typed
        name of the object consists of a semi colon separated list of
        all its fully classified class and sub-class names, a colon
        and the name to register the object by. The fully classifying
        names are ordered according to the objects MRO.
        """
        return ';'.join([obcn.__module__ + '.'+obcn.__name__
                        for obcn in obj.__class__.__mro__]) + ':' + obj_name

    def _decode_name(self, object_typed_name):
        """Return a pair of type names and object name. The types will be a
        tuple of all the fully qualifying types registered with the
        object.
        """
        types_string, obj_name = object_typed_name.split(':')
        type_name_list = types_string.split(';')
        return [type_name_list, obj_name]

    def _object_uri_by_name(self, obj_name):
        """Find the unique object whose name part is as given. The
        typed name and the object URI are returned as a pair.
        """
        obj_map = self._pns.list(regex='.*:{}$'.format(obj_name))
        self._pns._pyroRelease()
        if len(obj_map) == 0:
            raise Exception('The object with name "{}" was not registered.'
                            .format(obj_name))
        else:
            return list(obj_map.items())[0]

    def _object_tn_by_name(self, obj_name):
        """Get the fully typed name for the given obj_name."""
        type_list = list(self._pns.list(regex='.*:{}$'
                                        .format(obj_name)).keys())[0]
        self._pns._pyroRelease()
        return type_list

    def lookup(self, obj_name):
        """Lookup the object name and return the URI for it."""
        uri = self._object_uri_by_name(obj_name)[1]
        self._log.debug(
            'lookup: Object URI for name "{}" : "{}"'
            .format(obj_name, uri))
        if uri is None:
            self._log.warn(
                'lookup: The object name "{}" was not known by the name server'
                .format(obj_name))
        return uri

    def proxy(self, obj_name, timeout=None):
        """Like lookup, but construct and return a proxy over the
        found URI. If 'timeout' is set to a floating point number,
        that is how many seconds the method will wait for the
        'obj_name' to be available in the name server, retrying every
        second. If 'timeout' is set to 'None', only one attempt is
        performed. Upon success, the proxy is returned, and upon
        failure, an exception is raised.
        """
        # Check if timeout is given as a scalar
        if np.isscalar(timeout):
            timeout = np.float(timeout)
        elif timeout is None:
            timeout = 1.0
        else:
            self._log.warning(
                'Got a "timeout" which was neither numeric or None!' +
                ' Defaulting to 1 second.')
            timeout = 1.0
        # Account for one initial attempt
        timeout -= 1.0
        time_limit = time.time() + timeout
        # Make one initial attempt
        try:
            return Pyro4.Proxy(self.lookup(obj_name))
        except:
            pass
        # Re-attempt within time limit
        while time.time() < time_limit:
            try:
                # Use a lookup of the name; fixing the URI for the object
                return Pyro4.Proxy(self.lookup(obj_name))
            except:
                pass
            time.sleep(1.0)
        # No proxy could be made within the time limit
        raise Exception(self.__class__.__name__ +
                        ' : Could not locate an object with name "{}"'
                        .format(obj_name))

    def list(self, prefix=None, regex=None):
        """Mimic the Pyro4 name server's list method. Only match on name part
        of the registered strings, and only consider names with type
        prefixes. Returns a dict mapping from object name to object
        URI.
        """
        result = {}
        if prefix is not None:
            for tn, uri in self._pns.list(
                    regex='.*:{}'.format(prefix)).items():
                result[self._decode_name(tn)[1]] = uri
        elif regex is not None:
            for tn, uri in self._pns.list(
                    regex='.*:{}$'.format(regex)).items():
                result[self._decode_name(tn)[1]] = uri
        else:
            for tn, uri in self._pns.list(regex='.*:.*').items():
                result[self._decode_name(tn)[1]] = uri
        self._pns._pyroRelease()
        return result

    def register(self, obj_name, obj_uri, safe=False):
        """Register the object URI with a given object name,
        incorporating the object typed name into the raw Pyro name
        server name.
        """
        obj = self._dmn.objectsById[obj_uri.object]
        obj_tn = self._encode_name(obj, obj_name)
        self._pns.register(obj_tn, obj_uri, safe=safe)
        self._pns._pyroRelease()

    def register_object(self, obj_name, obj, safe=False):
        """Register the given object under the given name. The
        object will be registered with the local daemon, if it is
        not already registered. If safe is True, the object name
        is checked for existence, and object names will not be
        overwritten.
        """
        if safe:
            # Ensure that overwriting does not take place
            # try-except may be the most efficient method
            try:
                self.lookup(obj_name)
            except:
                pass
            else:
                raise Exception('The object name "{}" was already registered'
                                .format(obj_name))
        # Get the object URI
        if obj in self._dmn.objectsById.values():
            # The object was already registered
            obj_uri = self._dmn.uriFor(obj)
        else:
            # The object was not registered
            obj_uri = self._dmn.register(obj)
        # Type-encode the object name
        obj_tn = self._encode_name(obj, obj_name)
        # Register in base name server
        self._pns.register(obj_tn, obj_uri, safe=safe)
        self._pns._pyroRelease()

    def ping(self):
        """Ping through to the Pyro name server."""
        ping_result = self._pns.ping()
        self._pns._pyroRelease()
        return ping_result

    def remove(self, obj_name):
        """Remove the unique object with the given name."""
        self._pns.remove(self._object_uri_by_name(obj_name)[0])
        self._pns._pyroRelease()

    def get_object_names(self):
        return list(self.list().keys())
    object_names = property(get_object_names)

    def get_object_types(self, obj_name):
        """Return a list of type names for the obj_name."""
        return self._object_tn_by_name(obj_name).split(':')[0].split(';')

    def get_objects_of_type(self, obj_type_name, partial=True):
        """Return object names for all objects that has a type matching the
        given 'object_type_name'. If 'partial' is True, an object is
        returned if a last part of one of its fully qualifying type
        names matches. E.g. if 'partial' is True, an object that has
        one of its fully qualifying types as 'a.b.C' will have its
        object name returned by the queries 'a.b.C', 'b.C', and 'C',
        and only these. However, an object which has a fully
        qualifying type 'd.a.b.C' will also have its object name
        returned by these queries in this case. In the case where
        'partial' is False, an object name will only be returned if
        one of its fully qualifying type names matches exactly.

        """
        if partial:
            regex = '(^|.*(;|\.)){}($|;.*)'.format(obj_type_name)
        else:
            regex = '(^|.*;){}($|;.*)'.format(obj_type_name)
        match_objs = [otn.split(':')[-1] for otn in self._pns.list(
            regex=regex).keys()]
        self._pns._pyroRelease()
        return match_objs

    def get_type_names(self):
        types = set([])
        for obj_name in self.list():
            types |= set(self.object_types(obj_name))
        return types
    type_names = property(get_type_names)


def _test():
    """Test code for the typed name server"""

    import pyro4_utils as pu
    pu.init()

    class A(object):
        def __init__(self, ia):
            self._ia = ia

        def printa(self):
            print('a={}'.format(self._ia))
    a = A(2)

    class B(A):
        def __init__(self, ia, ib):
            A.__init__(self, ia)
            self._ib = ib

        def printab(self):
            print('a={} b={}'.format(self._ia, self._ib))

    b = B(3, 5)

    class C(A):
        def __init__(self, ia, ic):
            A.__init__(self, ia)
            self._ic = ic

        def printac(self):
            print('a={} c={}'.format(self._ia,
                                     self._ic))

    c = C(13, 8)

    ns = pu.name_server
    ns.register_object('a', a, safe=False)
    ns.register_object('b', b, safe=False)
    ns.register_object('c', c, safe=False)

    aprx = ns.proxy('a')
    aprx.printa()

    bprx = ns.proxy('b')
    bprx.printab()

    cprx = ns.proxy('c')
    cprx.printac()

    a_objs = ns.get_objects_of_type('A')
    for a in a_objs:
        ns.proxy(a).printa()

    print(ns.type_names)

    print(ns.list())
    ns.remove('a')
    print(ns.list())

    print(ns.object_types('b'))
    print(ns.object_types('c'))

    print(ns.type_names)
