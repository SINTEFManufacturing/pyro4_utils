# coding=utf-8

"""
Basic config of Pyro4
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2016"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import Pyro4

Pyro4.config.SERVERTYPE = 'thread'
Pyro4.config.SOCK_REUSE = True
Pyro4.config.SERIALIZERS_ACCEPTED = set(['pickle'])
Pyro4.config.SERIALIZER = 'pickle'
Pyro4.config.THREADPOOL_SIZE = 100
