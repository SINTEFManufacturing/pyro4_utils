#!/usr/bin/python3
# coding=utf-8

"""
"""

__author__ = "Morten Lind"
__copyright__ = "SINTEF 2014-2018"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten.lind@sintef.no"
__status__ = "Development"

import argparse
import logging
logging.basicConfig(level='DEBUG')


arg_parser = argparse.ArgumentParser('Pyro4 Name Server')

arg_parser.add_argument(
    '--name_server_host', type=str, default='0.0.0.0',
    help="""The address for which to start the Pyro4 name server. If left to
    default the name server is started on all interfaces.""")
arg_parser.add_argument(
    '--log_level', type=str,
    choices=list(logging._nameToLevel.keys()), default='INFO',
    help="""Set the logging level for the name server service.""")


args,_ = arg_parser.parse_known_args()

logging.basicConfig(level=args.log_level)

logging.info('Starting Pyro4 Name Server on {}'.format(args.name_server_host))

from pyro4_utils.config import Pyro4
import Pyro4.naming

Pyro4.naming.startNSloop(host=args.name_server_host)
