# coding=utf-8

"""Module for getting command line access to the basic Pyro utilities.
"""

__author__ = "Morten Lind"
__copyright__ = "Morten Lind, SINTEF 2014"
__credits__ = ["Morten Lind"]
__license__ = "GPLv3"
__maintainer__ = "Morten Lind"
__email__ = "morten@lind.dyndns.dk, morten.lind@sintef.no"
__status__ = "Development"

import argparse
import logging
import os
import atexit

import Pyro4
import pyro4_utils as pu

conf_ap = argparse.ArgumentParser()
conf_ap.add_argument('--conffile', action='store_true', default=False)
conf_args, _ = conf_ap.parse_known_args()

if conf_args.conffile:
    pu_conff = os.path.expandvars('$HOME/.pyro_dpcs/utils.conf')
    args = []
    for line in open(pu_conff):
        if line.find('=') >= 0:
            args.append(line.strip().split('='))
    pu_args = argparse.Namespace(**dict(args))
    logging.basicConfig(level=pu_args.log_level)
    # pu_args.log_level = 'INFO'
    # pu_args.topic_server_name = 'TopicServer'
    # pu_args.host = platform.node()
    # pu_args.daemon_host = '10.218.128.162'
    # pu_args.name_server_host = 'mlsrmpi'
    pu.init(pu_args)
else:
    pu.init()


class GenSub(object):
    """A generic subscriber which simply prints what is published."""
    def __init__(self, topic_name):
        pu.daemon.register(self)
        self.topic_name = topic_name
        self.topic = pu.topic_server.topic_map[self.topic_name]
        pu.topic_server.subscribe(self.topic_name, self)
        atexit.register(self.release)

    @Pyro4.callback
    @Pyro4.oneway
    def topic_callback(self, name, data):
        print(name, type(data), data)

    def release(self):
        pu.topic_server.unsubscribe(self.topic_name, self)

    def __del__(self):
        self.release()

import code
import rlcompleter
import readline
readline.parse_and_bind('tab: complete')
code.interact('Pyro4 Utils Environment', None, globals())
